
all:

upload: intervals.lua
	find . -not -path '*/\.*' -type f | xargs -P 5 -n 1 norns_upload gretchen/intervals
	norns_load gretchen/intervals intervals.lua

.PHONY: all upload

